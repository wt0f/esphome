#include "esphome.h"
#include "Adafruit_VEML6070.h"


class VEML6070CustomSensor : public PollingComponent, public Sensor {
 public:
  Adafruit_VEML6070 uv = Adafruit_VEML6070();
  VEML6070CustomSensor() : PollingComponent(15000) {}
  void setup() override {
    Wire.begin();
    uv.begin(VEML6070_1_T);
  }
  void update() override {
    uint16_t cur_uv = uv.readUV();
    ESP_LOGD("custom", "The value of sensor is: %i", cur_uv);
    publish_state(cur_uv);
  }
};

